import React from 'react'
import Webrouter from './components/routing/Webrouter'
import Discover from './components/web/screens/Discover'

function App() {
  return (

    <Webrouter />

  )
}

export default App
