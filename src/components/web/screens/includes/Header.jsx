import React from "react";
import styled from "styled-components";
import dropdown from "../../../../assets/images/Steyp-Designs/Iconly/Bold/Arrow - Down 2.svg";
import "../../../../assets/css/Style.css";

function Header() {
    return (
        <>
            <Head>
                <Wrapper>
                    <Lefthead>
                        <img
                            src={require("../../../../assets/images/hlogo.png")}
                            alt="Logo"
                        />
                    </Lefthead>
                    <Righthead>
                        <Categorydownbar>
                            <p>AllCategories</p>
                            <img src={dropdown} alt="Logo" />
                        </Categorydownbar>
                        <Searchbar>
                            <img
                                src={
                                    require("../../../../assets/images/icons8-search.svg")
                                      
                                }
                                alt="serach"
                            />
                            <input type="text" placeholder="Search" />
                        </Searchbar>
                        <Subscription>
                            <img
                                src={require("../../../../assets/images/language-icon.png")}
                                alt="image"
                            />
                            <span>Membership</span>
                            <span>Login</span>
                        </Subscription>
                    </Righthead>
                </Wrapper>
            </Head>
        </>
    );
}

export default Header;
const Head = styled.div`
    padding: 15px 0px;
    display: flex;
    justify-content: center;
    position: fixed;
    z-index: 20;
    width: 100%;
    background-color: hsl(0deg 0% 0%);
`;
const Wrapper = styled.div`
    width: 85%;
    display: flex;
    justify-content: space-between;
`;
const Lefthead = styled.div`
    width: 50px;
    align-items: center;
    display: flex;
    img {
        width: 100%;
        display: block;
    }
`;
const Righthead = styled.div`
    width: 65%;
    justify-content: space-evenly;

    display: flex;
`;
const Categorydownbar = styled.div`
    display: flex;
    align-items: center;
    cursor: pointer;

    p {
        color: #ddd;
        font-family: gordita_bold;
        font-size: 12px;

        margin-right: 6px;
    }
    img {
        width: 8px;
    }
`;
const Searchbar = styled.div`
    display: flex;
    align-items: center;
    border-radius: 15px;
    align-items: center;
    background-color: hsl(0deg 0% 10%);
    border-radius: 20px;
    width: 42%;
    padding: 9px 0px;
    img {
        width: 16px;
        margin-left: 16px;
      
    }
    input {
        font-size: 12px;
        font-family: gordita_medium;
        margin-left: 5px;
        ::placeholder {
            color: #fff;
            opacity: 1;
        }

    }
`;
const Subscription = styled.div`
    display: flex;
    align-items: center;
    & :nth-child(3) {
        margin-right: 0;
    }
    img {
        margin-right: 23px;
        width: 19px;
    }
    span {
        color: #ddd;
        font-family: gordita_bold;
        margin-right: 23px;
        font-size: 12px;
        cursor: pointer;
    }
`;
// const Head = styled.div``;
// const Head = styled.div``;
// const Head = styled.div``;
// const Head = styled.div``;
// const Head = styled.div``;
// const Head = styled.div``;
