import React from "react";
import styled from "styled-components";
import "../../../../src/assets/css/Style.css";
import image2 from "../../../assets/images/d1.png";
import image3 from "../../../assets/images/d2.png";
import image4 from "../../../assets/images/d3.png";
import image5 from "../../../assets/images/d4.png";
import image6 from "../../../assets/images/8kMU.png";
import Slider from "react-slick";

function Spotlight() {
    const sliding = [
        {
            id: 1,
     
            Image: image2,
        },
        {
            id: 2,
            Image: image3,
        },
        {
            id: 3,
            Image: image4,
        },
        {
            id: 4,
            Image: image5,
        },
        {
            id: 5,
            Image: image6,
        },
        {
            id: 6,
            Image: image2,
        },
        {
            id: 7,
            Image: image3,
        },
        {
            id: 8,
            Image: image4,
        },
        {
            id: 9,
            Image: image5,
        },
        {
            id: 10,
            Image: image6,
        },
    ];
    const settings = {
        dots: false,
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        speed: 2000,
        autoplaySpeed: 2000,
        cssEase: "linear",
        // adaptiveHeight: true
    };
    return (
        <>
            <Spotlightsection>
                <Leftcontainer>
                    <h1>Practical Learning from the best.</h1>
                    <p>
                        Lear from the most inspiring artist,leaders,and icons in
                        the world.
                    </p>
                    <div>
                        <button>Explore</button>
                    </div>
                </Leftcontainer>

                <Rightcontainer>
                    <ul>
                        <Slider {...settings}>
                            {sliding.map((value) => (
                                <li key={value.id}>
                                    <img src={value.Image} alt="image" />
                                </li>
                            ))}
                        </Slider>
                    </ul>
                </Rightcontainer>
            </Spotlightsection>
        </>
    );
}

export default Spotlight;
const Spotlightsection = styled.div`
    display: flex;
    padding-top: 10vh;
    /* height: 100vh; */
`;
const Leftcontainer = styled.div`
    padding: 70px 0px 70px 80px;
    color: #fff;
    width: 45%;
    flex-direction: column;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    h1 {
        font-size: 40px;
        font-family: "gordita_regular";
    }
    p {
        font-family: "gordita_regular";
        font-size: 17px;
}
    
    div {
        width: 100%;
        button {
            background-color: hsl(172deg 100% 39%);
            border-style: none;
            padding: 10px 31px;
            color: black;
            font-family: "gordita_medium";
            border-radius: 6px;
            border: 1px solid black;
            margin-top: 10px;
            cursor: pointer;
        }
    }
`;
const Rightcontainer = styled.div`
    width: 55%;

    ul {
        width: 95%;
        padding: 25px 25px;

        overflow-x: hidden;

        .slick-track {
            display: flex !important;
        }
        .slick-slide {
            height: inherit;
            display: flex !important;
        }
        .slick-slide > div {
            display: flex !important;
        }
        .slick-arrow {
            display: none !important;
        }
        & :nth-child(1) {
            margin-top: 0px;
        }
        li {
            width: 60%;
            margin-right: 40px;

            img {
                width: 100%;
                display: block;
                height: 100%;
            }
        }
    }
`;
// const Spotlightsection = styled.div``;
// const Spotlightsection = styled.div``;
// const Spotlightsection = styled.div``;
