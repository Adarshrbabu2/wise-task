import React from "react";
import Header from "./includes/Header";
import styled from "styled-components";
import Spotlight from "./Spotlight";
import Discover from "./Discover";
import "../../../assets/css/Style.css"
import Learn from "./Learn";

function WiseLandingPage() {
    return (
        <>
            <Container>
                <Header />
                <Spotlight /> 
                <Discover />
                <Learn />
            </Container>
        </>
    );
}

export default WiseLandingPage;
const Container = styled.div`
    background-color: hsl(0deg 0% 0%);

`;
