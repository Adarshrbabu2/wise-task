import React from "react";
import styled from "styled-components";

function Learn() {
    return (
        <>
            <Leransection>
                <Head>
                    <h1>What you will Discover ?</h1>
                </Head>
                <Topsection>
                    <Imagecontainer>
                    <img src={require("../../../assets/images/Apple-Macbook-Gold.png")} alt="arrow" />
                        </Imagecontainer>
                    <Coursetag>
                        <h2>Immensive Course</h2>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing
                            elit. Blanditiis facilis iure reprehenderit nostrum
                            quo quis tempora vel ut quam accusamus ipsum, sunt
                        </p>
                        <Page>
                            <span>Learn more</span>
                            <img src="#" alt="arrow" />
                        </Page>
                    </Coursetag>
                </Topsection>
                <Bottomsection>
                    <Coursetag>
                        <h2>Immensive Course</h2>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing
                            elit. Blanditiis facilis iure reprehenderit nostrum
                            quo quis tempora vel ut quam accusamus ipsum, sunt
                        </p>
                        <Page>
                            <span>Learn more</span>
                            <img src="#" alt="arrow" />
                        </Page>
                    </Coursetag>
                    <Imagecontainerright>
                    <img src={require("../../../assets/images/Apple-Macbook-Gold1.png")} alt="arrow" />
                    </Imagecontainerright>
                </Bottomsection>
            </Leransection>
        </>
    );
}

export default Learn;
const Leransection = styled.div`
padding-top: 100px;

`;
const Head = styled.div`
h1 {
    color: #fff;
    font-size: 49px;
    font-family: 'gordita_regular';
    max-width: 36%;
    margin: 0 auto;
    text-align: center;

}
`;
const Coursetag = styled.div`
position: absolute;
right: 0;
color: #fff;
background-color: hsl(218deg 14% 11%);
h2 {

}
p {

}

`;
const Imagecontainer = styled.div`
width: 70%;


img {
    width: 100%;
    display: block;

}

`;
const Page = styled.div`
span {

}
img {

}


`;
const Bottomsection = styled.div``;
const Topsection = styled.div`
display: flex;
position: relative;


`;
const Imagecontainerright = styled.div`
right: 0;
width:70% ;
img {
    width: 100%;
}

`;
// const Leransection = styled.div``;
// const Leransection = styled.div``;
// const Leransection = styled.div``;
// const Leransection = styled.div``;

