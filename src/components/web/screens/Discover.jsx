import React, { useState } from "react";
import styled from "styled-components";
import image2 from "../../../assets/images/d1.png";
import image3 from "../../../assets/images/d2.png";
import image4 from "../../../assets/images/d3.png";
import image5 from "../../../assets/images/d4.png";
import Slider from "react-slick";

function Discover() {
    const sliding = [
        {
            id: 1,
            Image: image2,
        },
        {
            id: 2,
            Image: image3,
        },
        {
            id: 3,
            Image: image4,
        },
        {
            id: 4,
            Image: image5,
        },

        {
            id: 5,
            Image: image2,
        },
        {
            id: 6,
            Image: image3,
        },
        {
            id: 7,
            Image: image4,
        },
        {
            id: 8,
            Image: image5,
        },
    ];

    function SamplePrevArrow(props) {
        const { className, style, onClick } = props;
        return (
            <div
                className={className}
                style={{ ...style, display: "block", background: "green" }}
                onClick={onClick}
            />
        );
    }
    const settings = {
        dots: false,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: false,
        speed: 2000,
        autoplaySpeed: 2000,
        cssEase: "linear",
        // adaptiveHeight: true
    };

    return (
        <>
            <Discoversction>
                <Topsection>
                    <Headsection>
                        <h1>Discover Lifelong Learning</h1>
                    </Headsection>
                    <Listsection>
                        <ul>
                            <li>ALL</li>
                            <li>FOOD</li>
                            <li>MUSIC</li>
                            <li>WRITING</li>
                            <li>BUSINESS</li>
                            <li>ARTS</li>
                        </ul>
                    </Listsection>
                </Topsection>

                <Bottomsection>
                    <ul>
                        <Slider {...settings}>
                            {sliding.map((value) => (
                                <li key={value.id}>
                                    <img src={value.Image} alt="image" />
                                </li>
                            ))}
                        </Slider>
                    </ul>
                </Bottomsection>
            </Discoversction>
        </>
    );
}

export default Discover;
const Discoversction = styled.div`
    padding-top: 60px;
    display: flex;
    flex-direction: column;
`;
const Topsection = styled.div``;
const Headsection = styled.div`
    h1 {
        color: #fff;
        font-family: "gordita_regular";
        max-width: 37%;
        margin: 0 auto;
        text-align: center;
        font-size: 40px;
    }
`;
const Listsection = styled.div`
    display: flex;
    justify-content: center;

    ul {
        color: #fff;
        display: flex;
        list-style: none;
        justify-content: space-evenly;
        width: 43%;
        padding-top: 30px;
        li {
            font-size: 13px;
            font-family: "gordita_bold";
            cursor: pointer;
        }
    }
`;
const Bottomsection = styled.div`
display: flex;
align-items: center;
justify-content: center;
padding-top: 20px;
    ul {
      width: 80%;
        overflow-x: hidden;

        .slick-track {
            display: flex !important;
        }
        .slick-slide {
            height: inherit;
            display: flex !important;
        }
        .slick-slide > div {
            display: flex !important;
        }
        .slick-arrow {
            display: none !important;
        }
        & :nth-child(1) {
            margin-top: 0px;
        }

        li {
            /* width: 20%; */
            margin-right: 20px;

            img {
                width: 100%;
                display: block;
            }
        }
    }

`;

// const Discoversction = styled.div``;
